//ACESSO SEQUENCIAL INDEXADO

#include<iostream>
#include<stdlib.h>
#include<stdio.h>
using namespace std;

#define ITENSPAGINA 21
#define MAXTABELA 100

typedef struct item{

    int chave;
    long int dado1;
    char dado2[500];

}Item;


int pesquisa(int* tabela, int tam, Item* item, FILE* arq){

	Item pagina[ITENSPAGINA];// vai  guardar a pagina onde estara o elemento buscado
	int i = 0;//referencia para a chave buscada(contador)
	long desloc;// deslocamento que sera feito ate a pagina da chave buscada
	Item aux;// item auxiliar
	//int j = 0;// contador auxiliar para percorer a pagina da chave 


	//procura a pagina onde o item pode se encontrar
	while(i<tam && tabela[i] <= item->chave){// o contador i passara a ter o valor do indice onde se inicia 
		i++;                                 // a pagina que contem o valor buscado
	}

	//caso a chave desejada seja menor que a 1a chave, o item  não existe no arquivo
	if(i==0){//quando o while nao rodar
		cout << "falha "<<endl;
		return 0;// falha
	}
	else if(i < tam){//atoa
		//cout << "i < tam"<<endl;
	}

	//lê a página desejada do arquivo
	desloc = (i-1)*(ITENSPAGINA)*sizeof(Item);//numero de bytes deslocados ate a pagina da chave 
	fseek(arq,desloc, SEEK_SET);// desloca a quantidade de 'desloc' apartir do inicio do arq
	

	//apos o deslocamento o ponteiro estara na pagina a ser lida 
	fread(pagina, sizeof(aux),ITENSPAGINA, arq);//le a pagina com os 'n' itens

	/*while( j < ITENSPAGINA){//le todos os itens de uma pagina

		fread(&aux, sizeof(aux), 1, arq);
		//cout << "Chave da pagina = "<< aux.chave<<endl;
		pagina[j] = aux;
		j++;
	}*/

	//pesquisa sequencial na página lida
	for(int j=0; j<ITENSPAGINA; j++){

		if(pagina[j].chave == item->chave){
			*item = pagina[j];
			cout << "ENCONTROU "<<endl;
			return 1;
		}
	}
	

	return 0;
}


int main(){

	int tabela[MAXTABELA];// tam tabela
	FILE* arq;
	Item x;
	int pos = 0;



	//abre o arquivo de dados e faz a verificacao
	arq = fopen("arquivo1.bin","rb");
	if(arq == NULL){
		cout << "Erro na abertura do arquivo..." <<endl;
		return 0;
	}

	fseek(arq,0,SEEK_END);
	int itens_restantes = (ftell(arq)/sizeof(Item)) % ITENSPAGINA;


	rewind(arq);//volta o ponteiro para o inicio do arquivo
	//gera a tabela de índice das páginas 
	while(fread(&x,sizeof(x),1,arq) == 1){

		fseek(arq,(ITENSPAGINA-1)*sizeof(x),SEEK_CUR);//pula as paginas
		tabela[pos] = x.chave;
		//cout <<"Chave = "<<x.chave << " pos no vetor : "<<pos <<endl; debugs
		pos++;
	}
	// le a pagina final incompleta quando houver
	if(itens_restantes != 0){
		fread(&x,sizeof(x),1,arq);
		tabela[pos] = x.chave;
		cout << "Indice final = "<< x.chave <<endl;
	}
		
	


	cout << "Numero de paginas : "<<pos <<endl;
	cout << "ITENS RESTANTES = " << itens_restantes << endl;

	do{

	cout << "\nDigite o codigo do livro desejado : "; //recebe a chave a ser buscada
	cin >> x.chave;

	//ativa a função de pesquisa
	if(pesquisa(tabela,pos,&x,arq) == 1){

		cout << "\nO LIVRO : "<< x.dado2 <<endl;
		cout << "CODIGO : "<< x.dado1 <<endl;
		cout << "CHAVE : "<< x.chave << endl;
		cout << "FOI ENCONTRADO "<<endl<<endl;
	}
	else
		cout << "\nO LIVRO DE CHAVE : "<<x.chave<<" NAO FOI ENCONTRADO "<<endl<<endl;

	}
	while(x.chave != 0);
	cout << "BUSCA ENCERRADA "<< endl;

	fclose(arq);

	return 0;
}