#include<iostream>
#include<stdlib.h>
#include<stdio.h>
using namespace std;

#define M 20
#define MM 2*M

typedef long TipoChave;

typedef struct TipoRegistro {
	TipoChave chave;
   	long int dado1;
    char dado2[500];

} TipoRegistro;

typedef struct TipoPagina* TipoApontador;

typedef struct TipoPagina {
	short n;
	TipoRegistro r[MM];//itens 2m
	TipoApontador p[MM+1];//ponteiros de itens 2m+1 

}TipoPagina;


void Inicializa (TipoApontador &Arvore)
{
	Arvore = NULL;
}

//PESQUISA
bool pesquisa(TipoRegistro* x, TipoApontador Ap){

	long i = 1;

	if(Ap == NULL){
		return false;
	}

	while(i<Ap->n && x->chave > Ap->r[i-1].chave){// pesquisa sequencial para encontrar o intervalo desejado
		i++;
	}

	if(x->chave == Ap->r[i-1].chave){//verifica se a chave desejada foi localizada
		*x = Ap->r[i-1];
		return true;
	}
	if(x->chave < Ap->r[i-1].chave){//ativacao recursiva da pesquisa em uma das sub-arvores(esq ou dir)

		return pesquisa(x, Ap->p[i-1]);
	}
	else{
		return pesquisa(x, Ap->p[i]);
	}
}


//INSERCAO
void insere_na_pagina(TipoApontador Ap, TipoRegistro Reg, TipoApontador ApDir){

	short nao_achou_posicao;
	int k;
	k = Ap->n;
	nao_achou_posicao = (k > 0);

	while(nao_achou_posicao){

		if(Reg.chave >= Ap->r[k-1].chave){
			nao_achou_posicao = false;
			break;
		}

		Ap->r[k] = Ap->r[k-1];
		Ap->p[k+1] = Ap->p[k];
		k--;

		if(k < 1){
			nao_achou_posicao = false;
		}

	}
	Ap->r[k] = Reg;
	Ap->p[k+1] = ApDir;
	Ap->n++;
}

void Ins(TipoRegistro Reg, TipoApontador Ap, short* cresceu, TipoRegistro* RegRetorno, TipoApontador* ApRetorno){

	long i = 1;
	long j;
	TipoApontador ApTemp;

	if(Ap == NULL){
		*cresceu = true;
		*RegRetorno = Reg;
		*ApRetorno = NULL;
		return;
	}

	while(i<Ap->n && Reg.chave > Ap->r[i-1].chave){
		i++;
	}

	if(Reg.chave == Ap->r[i-1].chave){
		cout << "Erro : Registro ja esta presente \n";
		*cresceu = false;
		return;
	}
	if(Reg.chave < Ap->r[i-1].chave){
		i--;
	}

	Ins(Reg, Ap->p[i], cresceu, RegRetorno, ApRetorno);

	if(!*cresceu){
		return;
	}
	if(Ap->n < MM ){//2m

	insere_na_pagina(Ap, *RegRetorno, *ApRetorno);
	*cresceu = false;
	return;
	}

	/* Overflow: Pagina tem que ser dividida*/
	ApTemp = (TipoApontador)malloc(sizeof(TipoPagina));
	ApTemp->n = 0;
	ApTemp->p[0] = NULL;

	if(i < M+1){
		insere_na_pagina(ApTemp, Ap->r[MM-1], Ap->p[MM]);/*2m-1, 2m*/
		Ap->n--;
		insere_na_pagina(Ap, *RegRetorno, *ApRetorno);
	}

	else{
		insere_na_pagina(ApTemp, *RegRetorno, *ApRetorno);
	}

	for( j = M+2; j<= MM; j++)//M+2, 2M

		insere_na_pagina(ApTemp, Ap->r[j-1], Ap->p[j]);
		Ap->n = M;//M
		ApTemp->p[0] = Ap->p[M+1];//M+1
		*RegRetorno = Ap->r[M];//M
		*ApRetorno = ApTemp;
	
}

void insere(TipoRegistro Reg, TipoApontador *Ap){

	short cresceu;
	TipoRegistro RegRetorno;
	TipoPagina *ApRetorno, *ApTemp;

	Ins(Reg, *Ap, &cresceu, &RegRetorno, &ApRetorno);

	if(cresceu){// Arvore cresce na altura pela raiz

		ApTemp = (TipoPagina*)malloc(sizeof(TipoPagina));
		ApTemp->n = 1;
		ApTemp->r[0] = RegRetorno;
		ApTemp->p[1] = ApRetorno;
		ApTemp->p[0] = *Ap;
		*Ap = ApTemp;
	}


}

int main(){

	TipoRegistro aux;

	TipoApontador Ap;
	TipoRegistro Reg;

	Inicializa(Ap);

	FILE* entrada;

	entrada = fopen("arquivo3.bin", "rb");

	if(entrada == NULL){
		cout << "Erro ao abrir arquivo... "<<endl;
	}
	
	int n = 1;
	while(fread(&Reg, sizeof(Reg), 1, entrada) == 1){// preenche a arvore B em memoria principal com os dados do arquivo

		cout << "N = "<< n << endl;
		insere(Reg, &Ap);
		n++;
	}

	do{

		cout << "\nDigite a chave do livro a buscar = ";
		cin >> aux.chave;

		if(pesquisa(&aux, Ap) == true){// se achar
			cout << "\nO LIVRO : "<< aux.dado2 <<endl;
			cout << "CODIGO : "<< aux.dado1 <<endl;
			cout << "CHAVE : "<< aux.chave << endl;
			cout << "FOI ENCONTRADO "<<endl<<endl;

		}
		else{
			cout << "\nO LIVRO DE CHAVE : " << aux.chave <<" NAO FOI ENCONTRADO NO CATALOGO" << endl;
		}
	}
	while(aux.chave != 0);
	cout << "\nPESQUISA ENCERRADA "<< endl;
   

	


	


	return 0;
}
