// GERADOR DE ARQUIVOS BINARIOS
// LEITOR DE ARQUIVOS BINARIOS

#include<iostream>
#include<stdlib.h>
#include<stdio.h>
using namespace std;

typedef long TipoChave;

typedef struct TipoRegistro{
	TipoChave chave;
   	long int dado1;
    char dado2[500];

}TipoRegistro;


int trocar(int a, int b){
	int c;

	c = a;
	a = b;
	b = c;
	return b;
}

void embaralhar(int *vet, int tam)
{
	for (int i = 0; i < tam; i++)
	{
		int r = rand() % tam;

		int temp = vet[i];
		vet[i] = vet[r];
		vet[r] = temp;
	}
}


bool gerar_arquivo(int quantidade, int situacao){

	FILE* arq;
	FILE* entrada;

	TipoRegistro reg;

	entrada = fopen("livros.txt", "r");
	if(entrada == NULL){// validação
    	cout << "O arquivo não pode ser aberto !! "<< endl;
    }

	srand(time(NULL));

	switch(situacao)
	{
		case 1:// Arquivo em ordem crescente
		{
			if((arq = fopen("arquivo1.bin", "wb")) == NULL){
				cout << "Falha ao criar o arquivo binario" << endl;
				return false;
			}

			for(int i = 1; i <= quantidade; i++)
			{
				cout<<"I = "<<i <<endl;
				reg.chave = i;	
				reg.dado1 = rand();
				fscanf(entrada," %s", reg.dado2);
				fwrite(&reg, sizeof(reg), 1, arq);
			}
			fclose(arq);
			return true;
		}	
		break;

		case 2:// Arquivo em ordem decrescente
		{
			if((arq = fopen("arquivo2.bin", "wb")) == NULL){
				cout << "Falha ao criar o arquivo binario" << endl;
				return false;
			}

			for(int i = quantidade ; i >= 1; i--)
			{
				cout << "I = "<< i << endl;
				reg.chave = i;	
				reg.dado1 = rand();
				fscanf(entrada," %s", reg.dado2);
				fwrite(&reg, sizeof(reg), 1, arq);
			}
			fclose(arq);
			return true;

		}	
		break;

		case 3:// gera arquivo aleatorio
		{
			int* vet = new int[quantidade];//vetor que sera embaralhado 

			for(int i=0; i<quantidade; i++){//preenche o vetor com numeros de 1 ate o tamanho total de itens
				vet[i] = i+1;
				//cout << "vet [i] = "<< vet[i] << endl;
			}
			embaralhar(vet, quantidade);//embaralha o vetor
			
			if((arq = fopen("arquivo3.bin", "wb")) == NULL){
				cout << "Falha ao criar o arquivo binario" << endl;
				return false;
			}

			for(int i = 1; i <= quantidade; i++)
			{	
				reg.chave = vet[i-1];// passa o numero aleatorio para a chave
				reg.dado1 = rand();
				cout << "CHAVE = "<< reg.chave <<endl;
				fscanf(entrada," %s", reg.dado2);
				fwrite(&reg, sizeof(reg), 1, arq);
			}
			fclose(arq);
			return true;
		}
		break;
	}
	fclose(entrada);//fecha o txt
	return true;
}

void ler (int situacao)
{
	FILE *arq;
	TipoRegistro reg;

	switch(situacao)
	{
		case 1:{

			if((arq = fopen("arquivo1.bin", "rb")) == NULL){
				cout << "Falaha ao abrir o arquivo" << endl;
				return;
			}

			while(fread(&reg, sizeof(reg), 1, arq) == 1){

				cout << "Chave: " << reg.chave << endl
					 << "Dado 1: " << reg.dado1 << endl
					 << "Dado 2: " << reg.dado2 << endl << endl;
			}
			fclose(arq);
		}
		break;

		case 2:{

			if((arq = fopen("arquivo2.bin", "rb")) == NULL)
			{
				cout << "Falaha ao abrir o arquivo" << endl;
				return;
			}

			while(fread(&reg, sizeof(reg), 1, arq) == 1)
			{
				cout << "Chave: " << reg.chave << endl
					 << "Dado 1: " << reg.dado1 << endl
					 << "Dado 2: " << reg.dado2 << endl << endl;
			}
			fclose(arq);
		}
		break;

		case 3:{

			if((arq = fopen("arquivo3.bin", "rb")) == NULL){
				cout << "Falha ao abrir o arquivo" << endl;
				return ;
			}

			while(fread(&reg, sizeof(reg), 1, arq) == 1){
				cout << "Chave:  " << reg.chave << endl
					 << "Dado 1: " << reg.dado1 << endl
					 << "Dado 2: " << reg.dado2 << endl << endl;
			}
			fclose(arq);
		}
		break;
	}
}

int main (){
	
	gerar_arquivo(1000, 2);
	//ler(1);
	return 0;
}